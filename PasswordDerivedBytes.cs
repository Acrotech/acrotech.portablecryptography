﻿using Acrotech.Exceptions;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableCryptography
{
    public class PasswordDerivedBytes
    {
        public const int DefaultIterationCount = 1000;
        public const int DefaultKeySize = 32;
        public const int DefaultIVSize = 16;
        public const Algorithms DefaultAlgorithm = default(Algorithms);
        public const PRNGDigestAlgorithm DefaultDigestAlgorithm = default(PRNGDigestAlgorithm);

        public PasswordDerivedBytes(string password, byte[] salt, int iterations = DefaultIterationCount, int keySize = DefaultKeySize, int ivSize = DefaultIVSize, Algorithms algorithm = DefaultAlgorithm)
            : this(Encoding.UTF8.GetBytes(password), salt, iterations, keySize, ivSize, algorithm)
        {
        }

        public PasswordDerivedBytes(byte[] password, byte[] salt, int iterations = DefaultIterationCount, int keySize = DefaultKeySize, int ivSize = DefaultIVSize, Algorithms algorithm = DefaultAlgorithm)
            : this(CreatePkcs5S2Generator(password, salt, iterations), keySize, ivSize, algorithm)
        {
        }

        public PasswordDerivedBytes(PbeParametersGenerator generator, int keySize = DefaultKeySize, int ivSize = DefaultIVSize, Algorithms algorithm = DefaultAlgorithm)
        {
            Generator = generator;
            CipherParameters = CreateCipherParameters(generator, keySize, ivSize, algorithm);
        }

        public PbeParametersGenerator Generator { get; private set; }
        public ICipherParameters CipherParameters { get; private set; }

        private ICipherParameters CreateCipherParameters(PbeParametersGenerator generator, int keySize = DefaultKeySize, int ivSize = DefaultIVSize, Algorithms algorithm = DefaultAlgorithm)
        {
            // note it doesn't matter which algorithm this is as we will always create the same parameter instance for any algorithm that isn't in the Algorithm enum
            const string DefaultAlgorithm = "AES";

            Throw.ThrowOnArgumentNull(() => generator);

            // the generator expects the key/iv size to be in bits (for some dumb reason), so we have to multiply by 8 before sending it to the generator
            return (algorithm == Algorithms.Default) ? 
                Generator.GenerateDerivedParameters(DefaultAlgorithm, keySize * 8, ivSize * 8) :
                Generator.GenerateDerivedParameters(algorithm.ToString(), keySize * 8, ivSize * 8);
        }

        #region GenerateSecureBytes

        public static byte[] GenerateSecureBytes(int length, PRNGDigestAlgorithm algorithm = DefaultDigestAlgorithm, long? seed = null)
        {
            return GenerateSecureBytes(length, CreatePRNG(algorithm, seed));
        }

        public static byte[] GenerateSecureBytes(int length, PRNGDigestAlgorithm algorithm = DefaultDigestAlgorithm, byte[] seed = null)
        {
            return GenerateSecureBytes(length, CreatePRNG(algorithm, seed));
        }

        public static byte[] GenerateSecureBytes(int length, SecureRandom prng)
        {
            var buffer = new byte[length];

            GenerateSecureBytes(buffer, 0, buffer.Length, prng);

            return buffer;
        }

        public static void GenerateSecureBytes(byte[] buffer, int offset, int length, SecureRandom prng)
        {
            Throw.ThrowOnArgumentNullOrEmpty(() => buffer);
            Throw.ThrowOnArgumentNull(() => prng);

            prng.NextBytes(buffer, offset, length);
        }

        #endregion

        #region PRNG Creators

        public static SecureRandom CreatePRNG(PRNGDigestAlgorithm algorithm = DefaultDigestAlgorithm, long? seed = null)
        {
            return CreatePRNG(algorithm, seed.HasValue ? x => x.SetSeed(seed.Value) : (Action<SecureRandom>)null);
        }

        public static SecureRandom CreatePRNG(PRNGDigestAlgorithm algorithm = DefaultDigestAlgorithm, byte[] seed = null)
        {
            return CreatePRNG(algorithm, seed != null ? x => x.SetSeed(seed) : (Action<SecureRandom>)null);
        }

        public static SecureRandom CreatePRNG(PRNGDigestAlgorithm algorithm = DefaultDigestAlgorithm, Action<SecureRandom> initAction = null)
        {
            var prng = SecureRandom.GetInstance(algorithm.ToString());

            if (initAction != null)
            {
                initAction(prng);
            }

            return prng;
        }

        #endregion

        #region Generator Creators

        public static Pkcs5S2ParametersGenerator CreatePkcs5S2Generator(byte[] password, byte[] salt, int iterations = DefaultIterationCount)
        {
            Throw.ThrowOnArgumentNullOrEmpty(() => password);
            Throw.ThrowOnArgumentNullOrEmpty(() => salt);

            return CreateAndInit<Pkcs5S2ParametersGenerator>(x => x.Init(password, salt, iterations));
        }

        private static T CreateAndInit<T>(Action<T> initAction = null)
            where T : new()
        {
            var instance = new T();

            if (initAction != null)
            {
                initAction(instance);
            }

            return instance;
        }

        #endregion

        public enum PRNGDigestAlgorithm
        {
            SHA1PRNG,
            SHA256PRNG
        }

        public enum Algorithms
        {
            Default,
            DES,
            DESEDE,
            RC2,
        }
    }
}
