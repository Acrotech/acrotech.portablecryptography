﻿using Acrotech.Exceptions;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Modes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableCryptography
{
    public static class BlockCipher
    {
        #region IAeadBlockCipher

        public static BufferedCipherBase CreateEncrypter(PasswordDerivedBytes pdb, IAeadBlockCipher cipher = null)
        {
            return Create(true, pdb, cipher);
        }

        public static BufferedCipherBase CreateDecrypter(PasswordDerivedBytes pdb, IAeadBlockCipher cipher = null)
        {
            return Create(false, pdb, cipher);
        }

        public static BufferedCipherBase Create(bool forEncryption, PasswordDerivedBytes pdb, IAeadBlockCipher cipher = null)
        {
            Throw.ThrowOnArgumentNull(() => pdb);

            var buffer = new BufferedAeadBlockCipher(cipher ?? CreateEaxBlockCipher());

            buffer.Init(forEncryption, pdb.CipherParameters);

            return buffer;
        }

        #endregion

        #region IBlockCipher

        public static BufferedCipherBase CreateEncrypter(PasswordDerivedBytes pdb, IBlockCipher cipher)
        {
            return Create(true, pdb, cipher);
        }

        public static BufferedCipherBase CreateDecrypter(PasswordDerivedBytes pdb, IBlockCipher cipher)
        {
            return Create(false, pdb, cipher);
        }

        public static BufferedCipherBase Create(bool forEncryption, PasswordDerivedBytes pdb, IBlockCipher cipher)
        {
            Throw.ThrowOnArgumentNull(() => pdb);
            Throw.ThrowOnArgumentNull(() => cipher);

            var buffer = new BufferedBlockCipher(cipher);

            buffer.Init(forEncryption, pdb.CipherParameters);

            return buffer;
        }

        public static EaxBlockCipher CreateEaxBlockCipher(IBlockCipher cipher = null)
        {
            return new EaxBlockCipher(cipher ?? CreateRijndaelCipher());
        }

        public static RijndaelEngine CreateRijndaelCipher(int blockSize = 16)
        {
            return new RijndaelEngine(blockSize * 8);
        }

        #endregion
    }
}
