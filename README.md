﻿# README #

When System.Security.Cryptography is not available for use in a portable class library, what do you do for cryptographic needs?

[BouncyCastle](http://bouncycastle.org/) provides a very comprehensive and feature rich cryptographic library that is fully compatible with all PCL profiles. The only problem is that without a solid understanding of cryptography and crypto-analysis the BouncyCastle library is a bit difficult to navigate and implement in your applications.

This library bridges the gap between the complex world of cryptography with BouncyCastle's implementations of these cryptographic algorithms, and the high level abstracted needs of every day developers. This library provides best-in-class default options (which are fully customizable for those with very specific needs) with minimal required knowledge of cryptography.

Currently this library supports a high level abstraction of three core cryptographic components.

### PasswordDerivedBytes ###

This class provides a very simple method of generating highly secure password to byte array conversions (for storing or comparing). This class uses by default the generally accepted best practice of [PBKDF2 (Password Based Key Derivation Function 2)](http://en.wikipedia.org/wiki/PBKDF2). PBKDF2 is implemented in the desktop profile(s) by Rfc2898DeriveBytes. The equivalent BouncyCastle class is Pkcs5S2ParametersGenerator (PKCS 5 V2.0 Scheme 2). The PortableCryptography equivalent class is simply named PasswordDerivedBytes (as you produce bytes from a text-based password). This class allows developers to safely create a secure byte array from a password that is generally safe against common crypto-attacks (such as rainbow tables).

### BlockCipher ###

This class provides the cryptographic engine for any implementation. BlockCipher's will read blocks of data and emit encrypted (or decrypted) data. This library uses buffered block ciphers to efficiently pipeline data into the engine, this eliminates the need for any data to be completely buffered in memory before beginning the cryptographic process. By default an [EAX Block Cipher](http://en.wikipedia.org/wiki/EAX_mode) is created (and wrapped in a buffered block cipher). This EAX block cipher provides maximum protection against crypto-analysis and cryptographic tampering. When creating a cipher block, the developer must provide a reference to the PasswordDerivedBytes that will be used to encrypt or decrypt the payload data.

### EncryptionCodec ###

This static class provides the high level simplification API to the cryptographic operations available in this library. Data can be encrypted from a byte buffer or via an input/output stream. All parameters for encyrption have generally accepted best choice defaults so that a developer wanting to perform crytographic encryption with minimal effort need only supply the bare minimums (the payload, and the password/salt).

---

## What is this repository for? ##

* Anyone who is writing a cross platform application and wants to safely encrypt and decrypt data or perform high quality text-password/byte conversions.

## Examples ##

#### Simple buffer based Encryption/Decryption ####

    byte[] buffer = null;
    byte[] salt = null;
    
    string password = null;
    
    // Load buffer, salt,  ...
    // Acquire password ...
    
    var encrypted = EncryptionCodec.Encrypt(buffer, password, salt);
    var decrypted = EncryptionCodec.Decrypt(encrypted, password, salt);

#### Stream based Encryption/Decryption ####

    // This would occur outside the PCL since FileInfo is not supported in PCLs
    FileInfo unencryptedFile = null;
    FileInfo encryptedFile = null;
    
    // Load files ...
    
    using (var input = unencryptedFile.OpenRead())
    using (var output = encryptedFile.OpenWrite())
    {
        EncryptionCodec.Encrypt(input, output, password, salt);
    }
    
    using (var input = encryptedFile.OpenRead())
    using (var output = unencryptedFile.OpenWrite())
    {
        EncryptionCodec.Decrypt(input, output, password, salt);
    }

---

## Who do I talk to? ##

* Contact me for suggestions, improvements, or bugs

## Changelog ##

#### 1.0.0.0 ####

* Initial Release
