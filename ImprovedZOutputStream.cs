﻿using Org.BouncyCastle.Utilities.Zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Acrotech.PortableCryptography
{
    public class ImprovedZOutputStream : ZOutputStream
    {
        public ImprovedZOutputStream(Stream output)
            : base(output)
        {
        }
        
        public ImprovedZOutputStream(Stream output, int level) 
            : base(output, level)
        {
        }

        public ImprovedZOutputStream(Stream output, int level, bool nowrap)
            : base(output, level, nowrap)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && base.closed == false)
            {
                try
                {
                    Finish();
                }
                catch (IOException)
                {
                }
                finally
                {
                    base.closed = true;
                    End();

                    // Don't dispose the output stream, we want to keep it alive so it's disposed organically
                    //base.output.Dispose();

                    base.output = null;
                }
            }
        }
    }
}
