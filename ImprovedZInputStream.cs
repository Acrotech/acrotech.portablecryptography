﻿using Org.BouncyCastle.Utilities.Zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Acrotech.PortableCryptography
{
    public class ImprovedZInputStream : ZInputStream
    {
        public ImprovedZInputStream(Stream input)
            : base(input)
        {
        }
        
        public ImprovedZInputStream(Stream input, bool nowrap)
            : base(input, nowrap)
        {
        }

        public ImprovedZInputStream(Stream input, int level)
            : base(input, level)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && base.closed == false)
            {
                base.closed = true;

                // Don't dispose the input stream, we want to keep it alive so it's disposed organically
                //base.input.Dispose();
            }
        }
    }
}
