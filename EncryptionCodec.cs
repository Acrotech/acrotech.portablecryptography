﻿using Acrotech.Exceptions;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Modes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Acrotech.PortableCryptography
{
    public static class EncryptionCodec
    {
        #region Encrypt

        public static byte[] Encrypt(byte[] data, string password, byte[] salt, int iterations = PasswordDerivedBytes.DefaultIterationCount,
            int keySize = PasswordDerivedBytes.DefaultKeySize, int ivSize = PasswordDerivedBytes.DefaultIVSize,
            PasswordDerivedBytes.Algorithms algorithm = PasswordDerivedBytes.DefaultAlgorithm)
        {
            byte[] buffer = null;

            using (var output = new MemoryStream())
            {
                Encrypt(data, output, password, salt, iterations, keySize, ivSize, algorithm);

                buffer = output.ToArray();
            }

            return buffer;
        }

        public static void Encrypt(byte[] data, Stream output, string password, byte[] salt, int iterations = PasswordDerivedBytes.DefaultIterationCount,
            int keySize = PasswordDerivedBytes.DefaultKeySize, int ivSize = PasswordDerivedBytes.DefaultIVSize,
            PasswordDerivedBytes.Algorithms algorithm = PasswordDerivedBytes.DefaultAlgorithm)
        {
            Throw.ThrowOnArgumentNullOrEmpty(() => data);

            using (var input = new MemoryStream(data))
            {
                Encrypt(input, output, password, salt, iterations, keySize, ivSize, algorithm);
            }
        }

        public static void Encrypt(Stream input, Stream output, string password, byte[] salt = null, int iterations = PasswordDerivedBytes.DefaultIterationCount, 
            int keySize = PasswordDerivedBytes.DefaultKeySize, int ivSize = PasswordDerivedBytes.DefaultIVSize,
            PasswordDerivedBytes.Algorithms algorithm = PasswordDerivedBytes.DefaultAlgorithm)
        {
            Throw.ThrowOnArgumentNullOrEmpty(() => password);
            Throw.ThrowOnArgumentNullOrEmpty(() => salt);

            Encrypt(input, output, new PasswordDerivedBytes(password, salt, iterations, keySize, ivSize, algorithm));
        }

        public static void Encrypt(Stream input, Stream output, PasswordDerivedBytes pdb, IAeadBlockCipher cipher = null)
        {
            Throw.ThrowOnArgumentNull(() => pdb);

            Encrypt(input, output, BlockCipher.CreateEncrypter(pdb, cipher));
        }

        public static void Encrypt(Stream input, Stream output, PasswordDerivedBytes pdb, IBlockCipher cipher)
        {
            Throw.ThrowOnArgumentNull(() => pdb);
            Throw.ThrowOnArgumentNull(() => cipher);

            Encrypt(input, output, BlockCipher.CreateEncrypter(pdb, cipher));
        }

        public static void Encrypt(Stream input, Stream output, BufferedCipherBase cipher)
        {
            Throw.ThrowOnArgumentNull(() => input);
            Throw.ThrowOnArgumentNull(() => output);
            Throw.ThrowOnArgumentNull(() => cipher);

            using (var encrypter = new ImprovedCipherStream(output, null, cipher))
            using (var compressor = new ImprovedZOutputStream(encrypter, 9))
            {
                input.CopyTo(compressor);
            }
        }

        #endregion

        #region Decrypt

        public static byte[] Decrypt(byte[] data, string password, byte[] salt, int iterations = PasswordDerivedBytes.DefaultIterationCount,
            int keySize = PasswordDerivedBytes.DefaultKeySize, int ivSize = PasswordDerivedBytes.DefaultIVSize,
            PasswordDerivedBytes.Algorithms algorithm = PasswordDerivedBytes.DefaultAlgorithm)
        {
            byte[] buffer = null;

            using (var output = new MemoryStream())
            {
                Decrypt(data, output, password, salt, iterations, keySize, ivSize, algorithm);

                buffer = output.ToArray();
            }

            return buffer;
        }

        public static void Decrypt(byte[] data, Stream output, string password, byte[] salt, int iterations = PasswordDerivedBytes.DefaultIterationCount,
            int keySize = PasswordDerivedBytes.DefaultKeySize, int ivSize = PasswordDerivedBytes.DefaultIVSize,
            PasswordDerivedBytes.Algorithms algorithm = PasswordDerivedBytes.DefaultAlgorithm)
        {
            Throw.ThrowOnArgumentNullOrEmpty(() => data);

            using (var input = new MemoryStream(data))
            {
                Decrypt(input, output, password, salt, iterations, keySize, ivSize, algorithm);
            }
        }

        public static void Decrypt(Stream input, Stream output, string password, byte[] salt, int iterations = PasswordDerivedBytes.DefaultIterationCount,
            int keySize = PasswordDerivedBytes.DefaultKeySize, int ivSize = PasswordDerivedBytes.DefaultIVSize,
            PasswordDerivedBytes.Algorithms algorithm = PasswordDerivedBytes.DefaultAlgorithm)
        {
            Throw.ThrowOnArgumentNullOrEmpty(() => password);
            Throw.ThrowOnArgumentNullOrEmpty(() => salt);

            Decrypt(input, output, new PasswordDerivedBytes(password, salt, iterations, keySize, ivSize, algorithm));
        }

        public static void Decrypt(Stream input, Stream output, PasswordDerivedBytes pdb, IAeadBlockCipher cipher = null)
        {
            Throw.ThrowOnArgumentNull(() => pdb);

            Decrypt(input, output, BlockCipher.CreateDecrypter(pdb, cipher));
        }

        public static void Decrypt(Stream input, Stream output, PasswordDerivedBytes pdb, IBlockCipher cipher)
        {
            Throw.ThrowOnArgumentNull(() => pdb);
            Throw.ThrowOnArgumentNull(() => cipher);

            Decrypt(input, output, BlockCipher.CreateDecrypter(pdb, cipher));
        }

        public static void Decrypt(Stream input, Stream output, BufferedCipherBase cipher)
        {
            Throw.ThrowOnArgumentNull(() => input);
            Throw.ThrowOnArgumentNull(() => output);
            Throw.ThrowOnArgumentNull(() => cipher);

            using (var decryptor = new ImprovedCipherStream(input, cipher, null))
            using (var compressor = new ImprovedZInputStream(decryptor))
            {
                compressor.CopyTo(output);
            }
        }

        #endregion
    }
}
