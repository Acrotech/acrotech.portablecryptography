﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Acrotech.PortableCryptography
{
    public class ImprovedCipherStream : CipherStream
    {
        public ImprovedCipherStream(Stream stream, IBufferedCipher readCipher, IBufferedCipher writeCipher)
            : base(stream, readCipher, writeCipher)
        {
            InternalStream = stream;
        }

        private Stream InternalStream { get; set; }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (WriteCipher != null)
                {
                    byte[] buffer = WriteCipher.DoFinal();
                    InternalStream.Write(buffer, 0, buffer.Length);
                    InternalStream.Flush();
                }

                // Don't dispose the internal stream, we want to keep it alive so it's disposed organically
                //InternalStream.Dispose();
            }
        }
    }
}
